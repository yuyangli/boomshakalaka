/***************************************************************************
Project:                XXXXXXX
(c) Copyright:       XXXXXXX
Company:            XXXXXXX

****************************************************************************/

/***************************************************************************
@File:                     pa_ecnr_pluginImpl.h
@Description:          XXXXXXX

****************************************************************************/

#ifndef PA_ECNR_PLUGIN_IMPL_H
#define PA_ECNR_PLUGIN_IMPL_H

#include "pa_ecnr_err.h"
#include "pa_ecnr_plugin.h"
#include <fstream>
#include <WinSock2.h>
#include <memory.h> 
using namespace std;

#define SEND_RECV_BUFFER_MAXSIZE sizeof(paECNR_Cmd)
#define SOCKET_BUFFER_MAXSIZE 4096
#define RINGBUFFER_SIZE (750 * SOCKET_BUFFER_MAXSIZE)

#define RecordAudioStreamIND(ecnrCmd) \
		if (ecnrCmd.cmd == PA_ECNR_AUDIO_STREAM_IND) { \
			switch (ecnrCmd.audioPath) { \
			case PA_ECNR_RECV_IN: \
				if(ecnrCmd.length > 0) { \
					pthis->fs.RecvIn.write(ecnrCmd.data, ecnrCmd.length); \
				} \
				pthis->m_leftBufSize.recvInBufSize = ecnrCmd.leftBufSize; \
				break; \
			case PA_ECNR_RECV_OUT: \
				if(ecnrCmd.length > 0) { \
					pthis->fs.RecvOut.write(ecnrCmd.data, ecnrCmd.length); \
				} \
				pthis->m_leftBufSize.recvOutBufSize = ecnrCmd.leftBufSize; \
				break; \
			case PA_ECNR_MIC_IN: \
				if(ecnrCmd.length > 0) { \
					pthis->fs.MicIn.write(ecnrCmd.data, ecnrCmd.length); \
				} \
				pthis->m_leftBufSize.micInBufSize = ecnrCmd.leftBufSize; \
				break; \
			case PA_ECNR_MIC_OUT: \
				if(ecnrCmd.length > 0) { \
					pthis->fs.MicOut.write(ecnrCmd.data, ecnrCmd.length); \
				} \
				pthis->m_leftBufSize.micOutBufSize = ecnrCmd.leftBufSize; \
				break; \
			case PA_ECNR_REF_IN: \
				if(ecnrCmd.length > 0) { \
					pthis->fs.RefIn.write(ecnrCmd.data, ecnrCmd.length); \
				} \
				pthis->m_leftBufSize.refInBufSize = ecnrCmd.leftBufSize; \
				break; \
			default: \
				break; \
			} \
		}

#define PCMConvertToWAVFile(recordPath) \
		FILE *fpS; \
		FILE *fpD; \
		char wsWavFileName[MAX_FOLDER_PATH_LEN] = {0}; \
		const char *wsPCMFileName = m_recordFilePath.recordPath; \
		memcpy(wsWavFileName, wsPCMFileName, strlen(wsPCMFileName)); \
		strcat_s(wsWavFileName, MAX_FOLDER_PATH_LEN, ".wav"); \
		fopen_s(&fpS, wsPCMFileName, "rb"); \
		fopen_s(&fpD, wsWavFileName, "wb+"); \
		if (fpS != NULL && fpD != NULL ) \
		{ \
			fseek(fpS, 0, SEEK_END); \
			long filelength = ftell(fpS); \
			ConvertToWavFile(fpD, filelength); \
			fseek(fpS, 0, SEEK_SET); \
			char buf[4]; \
			while (4 == fread(buf, 1, 4, fpS)) \
			{ \
				fwrite(buf, 1, 4, fpD); \
			} \
			fclose(fpS); \
			fclose(fpD); \
			remove(wsPCMFileName); \
		} \
		else { \
			m_errCallBack("Convert wav file failed"); \
		}

struct m_fstream
{
	fstream RecvIn;
	fstream RecvOut;
	fstream MicIn;
	fstream MicOut; 
	fstream RefIn;
};

struct leftBufferSize
{
    int recvInBufSize;
    int recvOutBufSize;
    int micInBufSize;
    int micOutBufSize;
    int refInBufSize;
};

enum cmdType
{
	PA_ECNR_GET_VERSION_REQ = 0x000001, // get version
	PA_ECNR_GET_VERSION_CFM = 0x010001,

	PA_ECNR_SET_PARAM_REQ = 0x000002, // set param
	PA_ECNR_SET_PARAM_CFM = 0x010002,

	PA_ECNR_GET_PARAM_REQ = 0x000003, // get param
	PA_ECNR_GET_PARAM_CFM = 0x010003,

	PA_ECNR_GET_RUNNING_STATE_REQ = 0x000004, // get running state 
	PA_ECNR_GET_RUNNING_STATE_CFM = 0x010004,

	PA_ECNR_SET_MUTE_PATH_REQ = 0x000005, // set mute path
	PA_ECNR_SET_MUTE_PATH_CFM = 0x010005,

	PA_ECNR_SET_MIC_OUTPUT_GAIN_REQ = 0x000006, // set mic output gain
	PA_ECNR_SET_MIC_OUTPUT_GAIN_CFM = 0x010006,

	PA_ECNR_SET_AUDIO_BUFFER_SIZE_REQ = 0x000007, // set audio bufferSize
	PA_ECNR_SET_AUDIO_BUFFER_SIZE_CFM = 0x010007,

	PA_ECNR_RECORD_AUDIO_START_REQ = 0x000008, // recode audio start
	PA_ECNR_RECORD_AUDIO_START_CFM = 0x010008,

	PA_ECNR_RECORD_AUDIO_STOP_REQ = 0x000009, // recode audio stop
	PA_ECNR_RECORD_AUDIO_STOP_CFM = 0x010009,

	PA_ECNR_AUDIO_STREAM_IND = 0x01000A, // audio stream ind

	PA_ECNR_INSEART_AUDIO_STREAM_REQ = 0x00000B, // inseart audio stream
	PA_ECNR_INSEART_AUDIO_STREAM_CFM = 0x01000B,

	PA_ECNR_BUFFER_ENOUGH = 0x00000C, // buffer over
    PA_ECNR_BUFFER_OVER   = 0x00000D, // buffer enough


    PA_ECNR_DISCONNECT = 0x00000E, // disconnect
	PA_ECNR_RECORD_BUFFER_END = 0x00000F
};

enum streamState {
	Start = 0x01,
	Continue = 0x02,
	End = 0x03
};

struct paECNR_Cmd{
	cmdType cmd;
	UINT16 status; // 0x0000~0xFFFF
	UINT16 length; // 0x0000~0x030
	int bufferSize; // 0x0000~0xFFFF
	int leftBufSize; // 0x0000 – 0x0400
	UINT8 state;  // 0x00 -0x04
	UINT8 path; // 0x00-0x04
	UINT8 audioPath; // 0x00-0x04
	float gain; // -24-24
	char streamBitMask;
	char acErr[128];
	char data[1024]; // config param
};

struct recordFilePath
{
    char recvInPath[MAX_FOLDER_PATH_LEN];
	char recvOutPath[MAX_FOLDER_PATH_LEN];
	char micInPath[MAX_FOLDER_PATH_LEN];
	char micOutPath[MAX_FOLDER_PATH_LEN];
	char refInPath[MAX_FOLDER_PATH_LEN];
};
 
/* FOllowing define PCM convert to WAV parameter */
struct RIFF_HEADER  
{  
    char szRiffID[4]; // 'R','I','F','F'  
    unsigned int dwRiffSize;  
    char szRiffFormat[4]; // 'W','A','V','E'  
};
 
struct WAVE_FORMAT  
{  
    unsigned short wFormatTag;  
    unsigned short wChannels;  
    unsigned int dwSamplesPerSec;  
    unsigned int dwAvgBytesPerSec;  
    unsigned short wBlockAlign;  
    unsigned short wBitsPerSample;  
};
 
struct FMT_BLOCK  
{  
    char szFmtID[4]; // 'f','m','t',' '  
    unsigned int dwFmtSize;  
    struct WAVE_FORMAT wavFormat;  
};
 
union DWORD_CHAR  
{  
    int nValue;  
    char charBuf[4];  
};

/* FOllowing define calss pa_ecnr_pluginImpl */
class pa_ecnr_pluginImpl
{
public:
	pa_ecnr_pluginImpl();
	~pa_ecnr_pluginImpl();

	paErrCode  paEcnrConnectServer(const ipaddress  rem_ip, const unsigned short port);
	paErrCode  paEcnrDisconnectServer();
	void* paEcnrRegErrorCallback(ecnr_errorCallBack errorCB);
	bool       paEcnrGetConStatus(void);
	paErrCode  paEcnrGetVersion(paEcnrVersion* version);
	ecnrRunningState  paEcnrGetRemProcess(void);
	paErrCode  paEcnrSetParam(paECNRParam* param, char* acErr);
	paErrCode  paEcnrGetParam(paECNRParam* param);
	/*paErrCode  paEcnrExportCfgFile(paECNRParam* param, const char* file);
	paErrCode  paEcnrImportCfgFile(const char* file,paECNRParam* param);*/
	paErrCode  paEcnrMuteAudioPath(audio_path audioPath);
	paErrCode  paEcnrSetMicGain(float gain);
	paErrCode  paEcnrSetRemoteStreamBufSize(const int bufSize);
	int        paEcnrGetLeftBufSize( audio_path audioPath);
	paErrCode  paEcnrStartRecordAudioStream(const char* folder, char streamBitMask, const char* commonName);
	paErrCode  paEcnrStopRecordAudioStream(void);
	paErrCode  paEcnrInseartAudioStream(const char* fllePath, audio_path audioPath);

	void m_errCallBack(char * reason);
	paErrCode dealCmd(paECNR_Cmd* ecnrCmd);

	static pa_ecnr_pluginImpl *getPa_ecnr_pluginImplInst();
	static DWORD WINAPI ThreadProcRecord(LPVOID lpvoid); // 录数据的线程
	static DWORD WINAPI ThreadProcInsert(LPVOID lpvoid); // 插数据的线程
private:
	bool m_bConnectState;

	// 判断是否正在录某个通道的数据
	bool m_bIsRecordRecvIn;
	bool m_bIsRecordMicIn;
	bool m_bIsRecordRefIn;
	bool m_bIsRecordRecvOut;
	bool m_bIsRecordMicOut;

	bool IsOpenInstFs; // 要插入HU的音频流文件是否打开
	bool IsOpenRcdFs; // 录取HU音频流的文件是否打开
	bool IsStart; // 是否是发送的插入HU的音频流的第一段数据

	bool IsDestroyThread; // 是否要销毁线程（与HU断开连接后，再次连接时，防止再次创建线程）
	bool IsSetBufferSize; // 是否设置过5个通道的buffer的大小

	static pa_ecnr_pluginImpl *m_pPa_ecnr_pluginImplInst;
	SOCKET SocketClientRecord; // 录数据的socket
	SOCKET SocketClientCommand; // 其他命令的socket
	SOCKET SocketClientInsert; // 插数据的socket

	ecnr_errorCallBack m_errorCB; // 返回错误的回调函数的指针
	paEcnrVersion* m_pVersion; // 存储版本号的数组指针
	paECNRParam* m_pParam; // 参数
	ecnrRunningState m_runningState; // HU运行状态

	recordFilePath m_recordFilePath; // 录取HU音频流的文件路径
	leftBufferSize m_leftBufSize; // 某个通道的buffer的剩余空间的大小
	m_fstream fs; // 录数据的文件流
	fstream m_InstFs; // 插入数据的文件流

	paECNR_Cmd m_ecnrInstCmd; // 插入命令

	char m_floder[MAX_FOLDER_PATH_LEN]; // 存储录数据的文件路径
	char m_commonName[MAX_FOLDER_PATH_LEN]; // 存储录数据的文件名
	char m_streamBitMask; // 通道的录取标志位

	char *m_acErr; // paEcnrSetParam(paECNRParam *param, char* acErr)的error值
	int setBufSize; // 设置RingBuffer的大小

	//双事件
	HANDLE m_hQuitEventRecord; 
	HANDLE m_hThreadCheckRecord;

	HANDLE m_hQuitEventInsert;
	HANDLE m_hThreadCheckInsert;

	HANDLE m_hCheckRecordComplete;

	HANDLE m_hThreadRecord; //record线程句柄
	HANDLE m_hThreadInsert; //insert线程句柄
	
	static int ConvertToWavFile(FILE *fp, long filelength);
};

#endif //PA_ECNR_PLUGIN_IMPL_H
//Eof

