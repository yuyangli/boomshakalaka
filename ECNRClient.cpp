// ECNRClient.cpp : Defines the entry point for the DLL application.
//
//#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include "pa_ecnr_plugin.h"
#include <fstream>
#include <string>
using namespace std;


//BOOL APIENTRY DllMain( HANDLE hModule, 
//					   DWORD  ul_reason_for_call, 
//					   LPVOID lpReserved
//					 )
//{
//	paEcnrConnectServer("192.168.24.115" , 8888);
//	//paEcnrInseartAudioStream("E:\123.txt", PA_ECNR_RECV_IN);
//	return TRUE;
//}

int main()
{
	/*ipaddress ip;
	ip[0] = 192;
	ip[1] = 168;
	ip[2] = 24;
	ip[3] = 115;*/
	//ipaddress serverIP;
	unsigned short port = 0;
	int ID = 0;
	paErrCode error;
	bool m_bConnectState = false;

	while (1) {
		cout << "ECNR Tuning Tool version 1.5,build for Hancock(Author Wangshan)" << endl;
		cout << "1. Connect			(paEcnrConnectServer)" << endl;
		cout << "2. Disconnect			(paEcnrDisconnectServer)" << endl;
		cout << "3. GetStatus			(paEcnrGetConStatus)" << endl;
		cout << "4. GetVersion			(paEcnrGetVersion)" << endl;
		cout << "5. GetRunningStatus		(paEcnrGetRemProcess)" << endl;
		cout << "6. GetParameters		(paEcnrGetParam)" << endl;
		cout << "7. GetLeftBufSize		(paEcnrGetLeftBufSize)" << endl;
		cout << "8. SetParameters		(paEcnrSetParam)" << endl;
		cout << "9. SetMuteAudioPath		(paEcnrMuteAudioPath)" << endl;
		cout << "10.SetMicGain			(paEcnrSetMicGain)" << endl;
		cout << "11.SetStreamBufSize		(paEcnrSetRemoteStreamBufSize)" << endl;
		cout << "12.StartRecord			(paEcnrStartRecordAudioStream)" << endl;
		cout << "13.StopRecord			(paEcnrStopRecordAudioStream)" << endl;
		cout << "14.SendData			(paEcnrInseartAudioStream)" << endl;

		cin >> ID;
		switch (ID) {
		case 1:
			{
				/*string IP;
				cout << "������Server IP��";
				cin >> IP;*/
				//memcpy(serverIP, IP.data(), IP.length());

				/*cout << "������Port��";
				cin >> port;*/
				error = paEcnrConnectServer(reinterpret_cast<const unsigned char *>("192.168.1.3"), 9999);
				switch (error)
				{
				case PA_ECNR_OK:
					cout << "paEcnrConnectServer: " << "PA_ECNR_OK" << endl;
					break;
				case PA_ECNR_FAILED:
					cout << "paEcnrConnectServer: " << "PA_ECNR_FAILED" << endl;
					break;
                case PA_FILE_NOT_EXIT:
					cout << "paEcnrConnectServer: " << "PA_FILE_NOT_EXIT" << endl;
					break;
				default:
					break;
				}
			}
			break;
		case 2:
			error = paEcnrDisconnectServer();
			switch (error)
			{
			case PA_ECNR_OK:
				cout << "paEcnrDisconnectServer: " << "PA_ECNR_OK" << endl;
				break;
			case PA_ECNR_FAILED:
				cout << "paEcnrDisconnectServer: " << "PA_ECNR_FAILED" << endl;
				break;
            case PA_FILE_NOT_EXIT:
				cout << "paEcnrDisconnectServer: " << "PA_FILE_NOT_EXIT" << endl;
				break;
			default:
				break; 
			}
			break;
		case 3:
			m_bConnectState = paEcnrGetConStatus();
			cout << "paEcnrGetConStatus: " << m_bConnectState << endl;
			break;
		case 4:
			{
				paEcnrVersion ver = {0};
				string str;
				error = paEcnrGetVersion(&ver);
				str = ver;
				switch (error)
				{
				case PA_ECNR_OK:
					cout << "paEcnrGetVersion: " << str << " PA_ECNR_OK" << endl;
					break;
				case PA_ECNR_FAILED:
					cout << "paEcnrGetVersion: " << str << " PA_ECNR_FAILED" << endl;
					break;
				case PA_FILE_NOT_EXIT:
					cout << "paEcnrGetVersion: " << str << " PA_FILE_NOT_EXIT" << endl;
					break;
				default:
					break;
				}
			}
			break;
		case 5:
			ecnrRunningState runStatue;
			runStatue = paEcnrGetRemProcess();
			switch (runStatue)
			{
			case PA_ECNR_NOT_INIT:
				cout << "paEcnrGetRemProcess: " << "PA_ECNR_NOT_INIT" << endl;
				break;
			case PA_ECNR_NOT_RUNNING:
				cout << "paEcnrGetRemProcess: " << "PA_ECNR_NOT_RUNNING" << endl;
				break;
			case PA_ECNR_PROCESS:
				cout << "paEcnrGetRemProcess: " << "PA_ECNR_PROCESS" << endl;
				break;
			default:
				break;
			}
			break;
		case 6:
			{
				string str;
				paECNRParam param;
				error = paEcnrGetParam(&param);
				str = param.data;
				switch (error)
				{
				case PA_ECNR_OK:
					cout << "paEcnrGetParam: " << str << " PA_ECNR_OK" << endl;
					break;
				case PA_ECNR_FAILED:
					cout << "paEcnrGetParam: " << str << " PA_ECNR_FAILED" << endl;
					break;
				case PA_FILE_NOT_EXIT:
					cout << "paEcnrGetParam: " << str << " PA_FILE_NOT_EXIT" << endl;
					break;
				default:
					break;
				}
			}
			break;
		case 7:
			{
				int bufSize;
				bufSize = paEcnrGetLeftBufSize(PA_ECNR_RECV_IN);
				cout << "paEcnrGetLeftBufSize: " << bufSize << endl;
			}
			break;
		case 8:
			{
				paECNRParam param;
				FILE* pFile = NULL;
				fopen_s(&pFile, "E:\\01_HDM_BT-WBS_test.cfg", "rb");
				int binnum = sizeof(param) / sizeof(int);
				fread(&param, sizeof(int), binnum, pFile);
				fclose(pFile);
				char acErr[128] = {0};
				error = paEcnrSetParam(&param, acErr);
				//cout << "paEcnrSetParam: " << error << endl;
				switch (error)
				{
				case PA_ECNR_OK:
					cout << "paEcnrSetParam: " << acErr << " PA_ECNR_OK" << endl;
					break;
				case PA_ECNR_FAILED:
					cout << "paEcnrSetParam: " << acErr << " PA_ECNR_FAILED" << endl;
					break;
				case PA_FILE_NOT_EXIT:
					cout << "paEcnrSetParam: " << acErr << " PA_FILE_NOT_EXIT" << endl;
					break;
				default:
					break;
				}
			}
			break;
		case 9:
			error = paEcnrMuteAudioPath(PA_ECNR_RECV_IN);
			switch (error)
			{
			case PA_ECNR_OK:
				cout << "paEcnrMuteAudioPath: " << "PA_ECNR_OK" << endl;
				break;
			case PA_ECNR_FAILED:
				cout << "paEcnrMuteAudioPath: " << "PA_ECNR_FAILED" << endl;
				break;
			case PA_FILE_NOT_EXIT:
				cout << "paEcnrMuteAudioPath: " << "PA_FILE_NOT_EXIT" << endl;
				break;
			default:
				break;
			}
			break;
		case 10:
			error = paEcnrSetMicGain(1);
			switch (error)
			{
			case PA_ECNR_OK:
				cout << "paEcnrSetMicGain: " << "PA_ECNR_OK" << endl;
				break;
			case PA_ECNR_FAILED:
				cout << "paEcnrSetMicGain: " << "PA_ECNR_FAILED" << endl;
				break;
			case PA_FILE_NOT_EXIT:
				cout << "paEcnrSetMicGain: " << "PA_FILE_NOT_EXIT" << endl;
				break;
			default:
				break;
			}
			break;
		case 11:
			error = paEcnrSetRemoteStreamBufSize(1024 * 1024);
			switch (error)
			{
			case PA_ECNR_OK:
				cout << "paEcnrSetRemoteStreamBufSize: " << "PA_ECNR_OK" << endl;
				break;
			case PA_ECNR_FAILED:
				cout << "paEcnrSetRemoteStreamBufSize: " << "PA_ECNR_FAILED" << endl;
				break;
			case PA_FILE_NOT_EXIT:
				cout << "paEcnrSetRemoteStreamBufSize: " << "PA_FILE_NOT_EXIT" << endl;
				break;
			default:
				break;
			}
			break;
		case 12:

			error = paEcnrStartRecordAudioStream("c:\\data\\", 
			      RECORD_RECVIN_STREAM
				  | RECORD_RECVOUT_STREAM
				  | RECORD_MICIN_STREAM
				  | RECORD_MICOUT_STREAM
				  | RECORD_REFIN_STREAM, 
				"file");
			switch (error)
			{
			case PA_ECNR_OK:
				cout << "paEcnrStartRecordAudioStream: " << "PA_ECNR_OK" << endl;
				break;
			case PA_ECNR_FAILED:
				cout << "paEcnrStartRecordAudioStream: " << "PA_ECNR_FAILED" << endl;
				break;
			case PA_FILE_NOT_EXIT:
				cout << "paEcnrStartRecordAudioStream: " << "PA_FILE_NOT_EXIT" << endl;
				break;
			default:
				break;
			}
			break;
		case 13:
			error = paEcnrStopRecordAudioStream();
			switch (error)
			{
			case PA_ECNR_OK:
				cout << "paEcnrStopRecordAudioStream: " << "PA_ECNR_OK" << endl;
				break;
			case PA_ECNR_FAILED:
				cout << "paEcnrStopRecordAudioStream: " << "PA_ECNR_FAILED" << endl;
				break;
			case PA_FILE_NOT_EXIT:
				cout << "paEcnrStopRecordAudioStream: " << "PA_FILE_NOT_EXIT" << endl;
				break;
			default:
				break;
			}
			break;
		case 14:
			error = paEcnrInseartAudioStream("c:\\data\\fileRecvInInsert.wav" , PA_ECNR_RECV_IN);
			switch (error)
			{
			case PA_ECNR_OK:
				cout << "paEcnrInseartAudioStream: " << "PA_ECNR_OK" << endl;
				break;
			case PA_ECNR_FAILED:
				cout << "paEcnrInseartAudioStream: " << "PA_ECNR_FAILED" << endl;
				break;
			case PA_FILE_NOT_EXIT:
				cout << "paEcnrInseartAudioStream: " << "PA_FILE_NOT_EXIT" << endl;
				break;
			default:
				break;
			}
			break;
		default:
			cout << "�˳���" << endl;
			system("pause");
			return 0;
		}
	}
}