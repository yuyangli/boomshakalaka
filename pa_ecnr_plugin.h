/***************************************************************************
Project:                XXXXXXX
(c) Copyright:       XXXXXXX
Company:            XXXXXXX

****************************************************************************/

/***************************************************************************
@File:                     pa_ecnr_plugin.h
@Description:          XXXXXXX
     
****************************************************************************/

/***************************************************************************
@File:                     pa_ecnr_api.h
@Description:          XXXXXXX

****************************************************************************/

#ifndef PA_ECNR_PlugIN_H
#define PA_ECNR_PlugIN_H

#include "pa_ecnr_err.h"

#ifdef __cplusplus
    extern "C" {
#endif

#define DllExport __declspec( dllexport )

#define MAX_FOLDER_PATH_LEN 256

#define RECORD_RECVIN_STREAM   (1)
#define RECORD_RECVOUT_STREAM   (1 << 1)
#define RECORD_MICIN_STREAM   (1 << 2)
#define RECORD_MICOUT_STREAM   (1 << 3)
#define RECORD_REFIN_STREAM   (1 << 4)

typedef char paEcnrVersion[100];
typedef  unsigned char ipaddress[4];

typedef enum
{
    PA_ECNR_RECV_IN,
    PA_ECNR_RECV_OUT,
    PA_ECNR_MIC_IN,
    PA_ECNR_MIC_OUT,
    PA_ECNR_REF_IN, 
}audio_path;

typedef enum
{
    PA_ECNR_NOT_INIT,
    PA_ECNR_NOT_RUNNING,
    PA_ECNR_PROCESS,
}ecnrRunningState;

/*Following defined the ecnr paramter*/
typedef struct
{
	char data[1024];
}paECNRParam;

typedef void (*ecnr_errorCallBack)(char * reason);

DllExport paErrCode  paEcnrConnectServer(const ipaddress rem_ip, const unsigned short port);
DllExport paErrCode  paEcnrDisconnectServer(void);
DllExport void*      paEcnrRegErrorCallback(ecnr_errorCallBack errorCB);
DllExport bool       paEcnrGetConStatus(void);
DllExport ecnrRunningState  paEcnrGetRemProcess(void);
DllExport paErrCode  paEcnrGetVersion(paEcnrVersion* version);
DllExport paErrCode  paEcnrSetParam(paECNRParam* param, char* acErr);
DllExport paErrCode  paEcnrGetParam(paECNRParam* param);
DllExport paErrCode  paEcnrMuteAudioPath(audio_path audioPath);
DllExport paErrCode  paEcnrSetMicGain(float gain);
DllExport paErrCode  paEcnrSetRemoteStreamBufSize(const int bufSize);
DllExport int        paEcnrGetLeftBufSize(audio_path audioPath);
DllExport paErrCode  paEcnrStartRecordAudioStream(const char* folder, char streamBitMask, const char* commonName);
DllExport paErrCode  paEcnrStopRecordAudioStream(void);
DllExport paErrCode  paEcnrInseartAudioStream(const char* fllePath, audio_path audioPath);

#ifdef __cplusplus
    }
#endif

#endif //PA_ECNR_PlugIN_H
//EOF
