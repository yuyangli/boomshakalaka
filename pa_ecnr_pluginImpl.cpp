﻿#include <iostream>
#include "pa_ecnr_pluginImpl.h"
#pragma comment(lib, "ws2_32.lib")
#pragma warning(disable : 4996)
using namespace std;

pa_ecnr_pluginImpl* pa_ecnr_pluginImpl::m_pPa_ecnr_pluginImplInst = NULL;

	static int rate = -1; // 录取的音频的频率
pa_ecnr_pluginImpl::pa_ecnr_pluginImpl()
	: SocketClientCommand(NULL),
	  SocketClientRecord(NULL),
	  SocketClientInsert(NULL),
	  m_bConnectState(false),
	  m_bIsRecordRecvIn(false),
	  m_bIsRecordMicIn(false),
	  m_bIsRecordRefIn(false),
	  m_bIsRecordRecvOut(false),
	  m_bIsRecordMicOut(false),
	  IsOpenInstFs(false),
	  IsOpenRcdFs(false),
	  IsStart(true),
	  IsDestroyThread(false),
	  IsSetBufferSize(false),
	  setBufSize(RINGBUFFER_SIZE),
	  m_pVersion(NULL),
	  m_runningState(PA_ECNR_NOT_RUNNING),
	  m_pParam(NULL),
	  m_streamBitMask(NULL),
	  m_errorCB(NULL),
	  m_hQuitEventRecord(NULL),
	  m_hThreadRecord(NULL),
	  m_hThreadCheckRecord(NULL),
	  m_hQuitEventInsert(NULL),
	  m_hThreadInsert(NULL),
	  m_hThreadCheckInsert(NULL),
	  m_acErr(NULL)
{
	
}

pa_ecnr_pluginImpl::~pa_ecnr_pluginImpl()
{
	paEcnrDisconnectServer();
}

pa_ecnr_pluginImpl * pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst()
{
	if (NULL == pa_ecnr_pluginImpl::m_pPa_ecnr_pluginImplInst) {
		pa_ecnr_pluginImpl::m_pPa_ecnr_pluginImplInst = new pa_ecnr_pluginImpl();
	}
	return pa_ecnr_pluginImpl::m_pPa_ecnr_pluginImplInst;
}

paErrCode  pa_ecnr_pluginImpl::paEcnrConnectServer(const ipaddress rem_ip, const unsigned short port)
{
	IsDestroyThread = false;
	paEcnrDisconnectServer();

	//1.加载库版本号
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;

	wVersionRequested = MAKEWORD(2, 2);

	err = WSAStartup(wVersionRequested, &wsaData);
	if (err != 0) {
		// 启动失败
		m_errCallBack("WSAStartup failed");
		return PA_ECNR_FAILED;
	}

	if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) {
		// 请求的库版本号不匹配
		WSACleanup();
		m_errCallBack("WSAStartup failed");
		return PA_ECNR_FAILED;
	}
	
	//2.创建socket
	SocketClientCommand = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(INVALID_SOCKET == SocketClientCommand) {
		WSACleanup();
		m_errCallBack("Create socket failed");
		return PA_ECNR_FAILED;
	}

	SocketClientRecord = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(INVALID_SOCKET == SocketClientRecord) {
		WSACleanup();
		m_errCallBack("Create socket failed");
		return PA_ECNR_FAILED;
	}

	SocketClientInsert = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(INVALID_SOCKET == SocketClientInsert) {
		WSACleanup();
		m_errCallBack("Create socket failed");
		return PA_ECNR_FAILED;
	}

	//int nNetTimeout = 2000;//2秒，
	//设置发送超时
	//setsockopt(SocketClientCommand, SOL_SOCKET, SO_SNDTIMEO, (char *)&nNetTimeout, sizeof(int));
	//setsockopt(SocketClientRecord, SOL_SOCKET, SO_SNDTIMEO, (char *)&nNetTimeout, sizeof(int));
	//setsockopt(SocketClientInsert, SOL_SOCKET, SO_SNDTIMEO, (char *)&nNetTimeout, sizeof(int));
	////设置接收超时
	//setsockopt(SocketClientCommand, SOL_SOCKET, SO_RCVTIMEO, (char *)&nNetTimeout, sizeof(int));
	//setsockopt(SocketClientRecord, SOL_SOCKET, SO_RCVTIMEO, (char *)&nNetTimeout, sizeof(int));
	//setsockopt(SocketClientInsert, SOL_SOCKET, SO_RCVTIMEO, (char *)&nNetTimeout, sizeof(int));

	//conncet
	sockaddr_in SockaddrClient;

	SockaddrClient.sin_family = AF_INET;
	SockaddrClient.sin_addr.S_un.S_addr = inet_addr((const char *)rem_ip);
	SockaddrClient.sin_port = htons(port);
	if(SOCKET_ERROR == connect(SocketClientCommand, (const sockaddr *)&SockaddrClient, sizeof(SockaddrClient)))
	{
		
		cout << "connect failed11" << WSAGetLastError() << endl;
		m_bConnectState = false;
		WSACleanup();
		closesocket(SocketClientCommand);
		m_errCallBack("connect failed");
		return PA_ECNR_FAILED;
	}
	
	if(SOCKET_ERROR == connect(SocketClientRecord, (const sockaddr *)&SockaddrClient, sizeof(SockaddrClient)))
	{
		cout << "connect failed22" << endl;
		m_bConnectState = false;
		WSACleanup();
		closesocket(SocketClientRecord);
		m_errCallBack("connect failed");
		return PA_ECNR_FAILED;
	}

	if(SOCKET_ERROR == connect(SocketClientInsert, (const sockaddr *)&SockaddrClient, sizeof(SockaddrClient)))
	{
		cout << "connect failed33" << endl;
		m_bConnectState = false;
		WSACleanup();
		closesocket(SocketClientInsert);
		m_errCallBack("connect failed");
		return PA_ECNR_FAILED;
	}
	
	m_bConnectState = true;

	char data[sizeof(int)] = {0};
	if (0 < recv(SocketClientCommand, data, sizeof(data), 0)) {
		memcpy(&rate, data, 4);
	}
	else {
		m_bConnectState = false;
		m_errCallBack("Get rate failed");
	}

	//创建退出事件
	if (!m_hQuitEventRecord) {
		m_hQuitEventRecord = CreateEvent(NULL, TRUE, FALSE, NULL);//退出事件
	}
	if (!m_hThreadCheckRecord) {
		m_hThreadCheckRecord = CreateEvent(NULL, TRUE, FALSE, NULL);
	}
	if (!m_hQuitEventInsert) {
		m_hQuitEventInsert = CreateEvent(NULL, TRUE, FALSE, NULL);//退出事件
	}
	if (!m_hThreadCheckInsert) {
		m_hThreadCheckInsert = CreateEvent(NULL, TRUE, FALSE, NULL);
	}

	//创建线程
	if (!m_hThreadRecord) {
		m_hThreadRecord = CreateThread(NULL, 0, &ThreadProcRecord, this, 0/*CREATE_SUSPENDED*/, NULL);
	}
	if (!m_hThreadInsert) {
		m_hThreadInsert = CreateThread(NULL, 0, &ThreadProcInsert, this, 0/*CREATE_SUSPENDED*/, NULL);
	}
	IsDestroyThread = true;

	return PA_ECNR_OK;
}

DWORD WINAPI pa_ecnr_pluginImpl::ThreadProcRecord(LPVOID lpvoid)
{
	pa_ecnr_pluginImpl* pthis = (pa_ecnr_pluginImpl*)lpvoid;
	
	paECNR_Cmd ecnrCmd;
	char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
	int recvLength = SEND_RECV_BUFFER_MAXSIZE;

	while(1)
	{
		if(WAIT_OBJECT_0 == WaitForSingleObject(pthis->m_hQuitEventRecord, 1))
		{
			break;
		}
/*
		if (!(pthis->IsOpenRcdFs) || !(pthis->m_bConnectState)) {
			if (pthis->m_bIsRecordRecvIn
				|| pthis->m_bIsRecordRecvOut
				|| pthis->m_bIsRecordMicIn
				|| pthis->m_bIsRecordMicOut
				|| pthis->m_bIsRecordRefIn) {
				ecnrCmd.cmd = PA_ECNR_RECORD_BUFFER_END;
				pthis->dealCmd(&ecnrCmd);
			}
			continue;
		}
		*/
		
		int rec = recv(pthis->SocketClientRecord, &(data[SEND_RECV_BUFFER_MAXSIZE - recvLength]), recvLength, 0);
		
		if (rec > 0) {
			if ((SEND_RECV_BUFFER_MAXSIZE == rec) || (rec == recvLength)) {
				memcpy(&ecnrCmd, data, SEND_RECV_BUFFER_MAXSIZE);


				RecordAudioStreamIND(ecnrCmd);			

				
				pthis->dealCmd(&ecnrCmd);
				recvLength = SEND_RECV_BUFFER_MAXSIZE;
				memset(data, 0, SEND_RECV_BUFFER_MAXSIZE);
			}
			else {
				memcpy(&ecnrCmd, data, SEND_RECV_BUFFER_MAXSIZE);
				pthis->dealCmd(&ecnrCmd);
				recvLength = recvLength - rec;
			}

		}
		else {
			pthis->m_bConnectState = false;
		}
	}
	SetEvent(pthis->m_hThreadCheckRecord);
	return 0;
}

DWORD WINAPI pa_ecnr_pluginImpl::ThreadProcInsert(LPVOID lpvoid)
{
	pa_ecnr_pluginImpl* pthis = (pa_ecnr_pluginImpl*)lpvoid;
	char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
	char err[SEND_RECV_BUFFER_MAXSIZE] = {PA_ECNR_BUFFER_ENOUGH};

	streamsize sztrue = 0;
	paECNR_Cmd bufferStatusCmd;

	while(1)
	{
		if(WAIT_OBJECT_0 == WaitForSingleObject(pthis->m_hQuitEventInsert, 5))
		{
			break;
		}

		if (!(pthis->IsOpenInstFs) || !(pthis->m_bConnectState)) {
			continue;
		}

		memcpy(&bufferStatusCmd, err, SEND_RECV_BUFFER_MAXSIZE);
		if (bufferStatusCmd.cmd == PA_ECNR_BUFFER_ENOUGH) {
			pthis->m_InstFs.read(pthis->m_ecnrInstCmd.data, sizeof(pthis->m_ecnrInstCmd.data));
			sztrue = pthis->m_InstFs.gcount();
		}
		else if (bufferStatusCmd.cmd == PA_ECNR_BUFFER_OVER) {
			cout << "buffer over" << endl;
		}

		pthis->m_ecnrInstCmd.length = (UINT16)sztrue;

		if(sztrue > 0 && pthis->IsOpenInstFs) {
			if (pthis->IsStart) {
				pthis->m_ecnrInstCmd.state = Start;
				pthis->IsStart = false;
			}
			else {
				pthis->m_ecnrInstCmd.state = Continue;
			}

			memset(data, 0, SEND_RECV_BUFFER_MAXSIZE); 
			memcpy(data, &pthis->m_ecnrInstCmd, sizeof(pthis->m_ecnrInstCmd));
			int errS = send(pthis->SocketClientInsert, data, sizeof(data), 0);
			if (errS <= 0) {
				pthis->m_bConnectState = false;
			}
			memset(err, NULL, SEND_RECV_BUFFER_MAXSIZE);
			int errR = recv(pthis->SocketClientInsert, err, sizeof(err), 0);
			if (errR <= 0) {
				pthis->m_bConnectState = false;
			}
		}
		else {
			pthis->m_ecnrInstCmd.state = End;
			memset(pthis->m_ecnrInstCmd.data, 0, sizeof(pthis->m_ecnrInstCmd.data));
			memcpy(data, &pthis->m_ecnrInstCmd, sizeof(pthis->m_ecnrInstCmd));
			int snd = send(pthis->SocketClientInsert, data, sizeof(data), 0);
			if (snd <= 0) {
				pthis->m_bConnectState = false;
			}
			memset(data, NULL, SEND_RECV_BUFFER_MAXSIZE);
			int rec = recv(pthis->SocketClientInsert, data, sizeof(data), 0);
			if (rec > 0) {
				memcpy(&pthis->m_ecnrInstCmd, data, sizeof(data));
				pthis->dealCmd(&pthis->m_ecnrInstCmd);
			}
			else {
				pthis->m_bConnectState = false;
			}
			pthis->m_InstFs.close();
			pthis->IsOpenInstFs = false;
		}
	}
	SetEvent(pthis->m_hThreadCheckInsert);
	return 0;
}

paErrCode pa_ecnr_pluginImpl::paEcnrInseartAudioStream(const char *fllePath, audio_path audioPath)
{
	if (!m_bConnectState) {
		return PA_ECNR_FAILED;
	}
	memset(&(m_ecnrInstCmd), NULL, SEND_RECV_BUFFER_MAXSIZE);
	m_ecnrInstCmd.cmd = PA_ECNR_INSEART_AUDIO_STREAM_REQ;
	m_ecnrInstCmd.audioPath = audioPath;

	m_InstFs.close();
	IsOpenInstFs = false;

	m_InstFs.open(fllePath, fstream::in | fstream::binary);

	if(!m_InstFs) {
		//cout << "打开文件失败" << endl;
		// 打开文件失败
		IsOpenInstFs = false;
		m_errCallBack("open file failed");
		return PA_FILE_NOT_EXIT;
	}
	else {
		char data[4] = {0};
		m_InstFs.read(data, 4);
		if (0 == strncmp(data, "RIFF", 4))
		{
			m_InstFs.seekg(44, fstream::beg);
		}
		else {
			m_InstFs.seekg(0, fstream::beg);
		}
		IsStart = true;
		IsOpenInstFs = true;
		return PA_ECNR_OK;
	}
}

bool pa_ecnr_pluginImpl::paEcnrGetConStatus()
{
	return m_bConnectState;
}

paErrCode pa_ecnr_pluginImpl::paEcnrGetVersion(paEcnrVersion *version)
{
	if (version == NULL || *version == NULL) {
		m_errCallBack("get failed");
		return PA_ECNR_FAILED;
	}
	else {
		m_pVersion = version;
	}

	paECNR_Cmd ecnrCmd;
	char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
	ecnrCmd.cmd = PA_ECNR_GET_VERSION_REQ;

	memcpy(data, &ecnrCmd, SEND_RECV_BUFFER_MAXSIZE);

	if ( 0 >= send(SocketClientCommand, data, sizeof(data), 0)) {
	    m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
	 
	memset(data, NULL, sizeof(data));
	memset(&ecnrCmd, NULL, SEND_RECV_BUFFER_MAXSIZE);

	if (0 < recv(SocketClientCommand, data, sizeof(data), 0)) {
		memcpy(&ecnrCmd, data, SEND_RECV_BUFFER_MAXSIZE);
		return dealCmd(&ecnrCmd);
	}else {
		// 抛出错误
	    m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
}

ecnrRunningState pa_ecnr_pluginImpl::paEcnrGetRemProcess()
{
	paECNR_Cmd ecnrCmd;
	char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
	ecnrCmd.cmd = PA_ECNR_GET_RUNNING_STATE_REQ;

	memcpy(data, &ecnrCmd, SEND_RECV_BUFFER_MAXSIZE);

	if (0 >= send(SocketClientCommand, data, sizeof(data), 0)) {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_NOT_RUNNING;
	}
	 
	memset(data, NULL, sizeof(data));
	memset(&ecnrCmd, NULL, SEND_RECV_BUFFER_MAXSIZE);

	if (0 < recv(SocketClientCommand, data, sizeof(data), 0)) {
		memcpy(&ecnrCmd, data, SEND_RECV_BUFFER_MAXSIZE);
		dealCmd(&ecnrCmd);
		return m_runningState;
	}else {
		// 抛出错误
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_NOT_RUNNING;
	}
}

int pa_ecnr_pluginImpl::paEcnrGetLeftBufSize(audio_path audioPath)
{
	if (!m_bConnectState) {
		if (IsSetBufferSize) {
			return setBufSize;
		}
		else {
			return setBufSize;
		}
	}

	if (!IsOpenRcdFs && !IsOpenInstFs) {
		return setBufSize;
	}

	switch (audioPath) {
		case PA_ECNR_RECV_IN:
			return m_leftBufSize.recvInBufSize;
			
		case PA_ECNR_RECV_OUT:
			return m_leftBufSize.recvOutBufSize;
			
		case PA_ECNR_MIC_IN:
			return m_leftBufSize.micInBufSize;
			
		case PA_ECNR_MIC_OUT:
			return m_leftBufSize.micOutBufSize;
			
		case PA_ECNR_REF_IN:
			return m_leftBufSize.refInBufSize;

		default:
			break;
		}
	m_errCallBack("get failed");
	return -1;
}

paErrCode pa_ecnr_pluginImpl::paEcnrGetParam(paECNRParam *param)
{
	if (param == NULL) {
		m_errCallBack("get failed");
		return PA_ECNR_FAILED;
	}
	else {
		m_pParam = param;
	}

	paECNR_Cmd ecnrCmd;
	char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
	ecnrCmd.cmd = PA_ECNR_GET_PARAM_REQ;

	memcpy(data, &ecnrCmd, SEND_RECV_BUFFER_MAXSIZE);

	
	if (0 >= send(SocketClientCommand, data, sizeof(data), 0)) {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
	 
	memset(data, NULL, sizeof(data));
	memset(&ecnrCmd, NULL, SEND_RECV_BUFFER_MAXSIZE);

	if (0 < recv(SocketClientCommand, data, sizeof(data), 0)) {
		memcpy(&ecnrCmd, data, SEND_RECV_BUFFER_MAXSIZE);
		return dealCmd(&ecnrCmd);
	}else {
		// 抛出错误
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
}

paErrCode pa_ecnr_pluginImpl::paEcnrSetParam(paECNRParam *param, char* acErr)
{
	if (param == NULL) {
		m_errCallBack("set failed");
		return PA_ECNR_FAILED;
	}
	m_acErr = acErr;
	paECNR_Cmd ecnrCmd;
	char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
	ecnrCmd.cmd = PA_ECNR_SET_PARAM_REQ;
	ecnrCmd.length = 1024;
	memcpy(ecnrCmd.data, param->data, sizeof(param->data));

	memcpy(data, &ecnrCmd, SEND_RECV_BUFFER_MAXSIZE);

	 
	if (0 >= send(SocketClientCommand, data, sizeof(data), 0)) {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}

	memset(data, NULL, sizeof(data));
	memset(&ecnrCmd, NULL, SEND_RECV_BUFFER_MAXSIZE);

	if (0 < recv(SocketClientCommand, data, sizeof(data), 0)) {
		memcpy(&ecnrCmd, data, SEND_RECV_BUFFER_MAXSIZE);
		return dealCmd(&ecnrCmd);
	}else {
		// 抛出错误
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
}

paErrCode pa_ecnr_pluginImpl::paEcnrMuteAudioPath(audio_path audioPath)
{
	paECNR_Cmd ecnrCmd;
	char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
	ecnrCmd.cmd = PA_ECNR_SET_MUTE_PATH_REQ;
	ecnrCmd.audioPath = audioPath;

	memcpy(data, &ecnrCmd, SEND_RECV_BUFFER_MAXSIZE);

	
	if (0 >= send(SocketClientCommand, data, sizeof(data), 0)) {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
	 
	memset(data, NULL, sizeof(data));
	memset(&ecnrCmd, NULL, SEND_RECV_BUFFER_MAXSIZE);

	if (0 < recv(SocketClientCommand, data, sizeof(data), 0)) {
		memcpy(&ecnrCmd, data, SEND_RECV_BUFFER_MAXSIZE);
		return dealCmd(&ecnrCmd);
	}else {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
}

paErrCode pa_ecnr_pluginImpl::paEcnrSetMicGain(float gain)
{
	paECNR_Cmd ecnrCmd;
	char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
	ecnrCmd.cmd = PA_ECNR_SET_MIC_OUTPUT_GAIN_REQ;
	ecnrCmd.gain = gain;

	memcpy(data, &ecnrCmd, SEND_RECV_BUFFER_MAXSIZE);

	if (0 >= send(SocketClientCommand, data, sizeof(data), 0)) {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
	 
	memset(data, NULL, sizeof(data));
	memset(&ecnrCmd, NULL, SEND_RECV_BUFFER_MAXSIZE);

	if (0 < recv(SocketClientCommand, data, sizeof(data), 0)) {
		memcpy(&ecnrCmd, data, SEND_RECV_BUFFER_MAXSIZE);
		return dealCmd(&ecnrCmd);
	}else {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
}

paErrCode pa_ecnr_pluginImpl::paEcnrSetRemoteStreamBufSize(const int bufSize)
{

	if (IsOpenInstFs || IsOpenRcdFs) {
		m_errCallBack("Recording or inserting audio stream");
		return PA_ECNR_FAILED;
	}

	setBufSize = bufSize;

	paECNR_Cmd ecnrCmd;
	char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
	ecnrCmd.cmd = PA_ECNR_SET_AUDIO_BUFFER_SIZE_REQ;
	ecnrCmd.bufferSize = bufSize;

	memcpy(data, &ecnrCmd, SEND_RECV_BUFFER_MAXSIZE);

	if (0 >= send(SocketClientCommand, data, sizeof(data), 0)) {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
	 
	memset(data, NULL, sizeof(data));
	memset(&ecnrCmd, NULL, SEND_RECV_BUFFER_MAXSIZE);

	if (0 < recv(SocketClientCommand, data, sizeof(data), 0)) {
		memcpy(&ecnrCmd, data, SEND_RECV_BUFFER_MAXSIZE);
		return dealCmd(&ecnrCmd);
	}else {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
}

paErrCode pa_ecnr_pluginImpl::paEcnrStartRecordAudioStream(const char *folder, char streamBitMask, const char *commonName)
{
	if (folder == NULL || commonName == NULL) {
		m_errCallBack("Record failed");
		return PA_ECNR_FAILED;
	}

	IsOpenRcdFs = false;

	m_bIsRecordMicIn = false;
	m_bIsRecordMicOut = false;
	m_bIsRecordRecvIn = false;
	m_bIsRecordRecvOut = false;
	m_bIsRecordRefIn = false;

	fs.RecvIn.close();
	fs.RecvOut.close();
	fs.MicIn.close();
	fs.MicOut.close();
	fs.RefIn.close();

	int a = strlen(folder);
	memset(m_floder, NULL, sizeof(m_floder));
	memset(m_commonName, NULL, sizeof(m_commonName));
	memcpy(m_floder, folder, strlen(folder));
	m_streamBitMask = streamBitMask;
	memcpy(m_commonName, commonName, strlen(commonName));

	paECNR_Cmd ecnrCmd;
	char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
	ecnrCmd.cmd = PA_ECNR_RECORD_AUDIO_START_REQ;
	ecnrCmd.streamBitMask = streamBitMask;

	memcpy(data, &ecnrCmd, SEND_RECV_BUFFER_MAXSIZE);

	if (0 >= send(SocketClientCommand, data, sizeof(data), 0)) {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}

	if ((m_streamBitMask & 0x1F) == 0) {
		m_errCallBack("Record streamBitNask is 0");
		return PA_ECNR_FAILED;
	}

	memset(data, NULL, sizeof(data));
	memset(&ecnrCmd, NULL, SEND_RECV_BUFFER_MAXSIZE);

	if (0 < recv(SocketClientCommand, data, sizeof(data), 0)) {
		memcpy(&ecnrCmd, data, SEND_RECV_BUFFER_MAXSIZE);
		return dealCmd(&ecnrCmd);
	}else {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
}

paErrCode pa_ecnr_pluginImpl::paEcnrStopRecordAudioStream()
{
		//cout << "PA_ECNR_GET_VERSION_CFM" << endl;
	

	

	paECNR_Cmd ecnrCmd;
	char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
	ecnrCmd.cmd = PA_ECNR_RECORD_AUDIO_STOP_REQ;

	memcpy(data, &ecnrCmd, SEND_RECV_BUFFER_MAXSIZE);
	
	int snd = send(SocketClientCommand, data, sizeof(data), 0);
	if (0 >= snd) {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
	 
	memset(data, NULL, sizeof(data));
	memset(&ecnrCmd, NULL, SEND_RECV_BUFFER_MAXSIZE);

	int rec = recv(SocketClientCommand, data, sizeof(data), 0);
	if (0 < rec) {
		memcpy(&ecnrCmd, data, SEND_RECV_BUFFER_MAXSIZE);
		return dealCmd(&ecnrCmd);
	}else {
		m_bConnectState = false;
		m_errCallBack("Disconnect");
		return PA_ECNR_FAILED;
	}
}

paErrCode pa_ecnr_pluginImpl::dealCmd(paECNR_Cmd* ecnrCmd)
{
	if (NULL == ecnrCmd) {
		m_errCallBack("Command is NULL");
		return PA_ECNR_FAILED;
	}
	
	switch(ecnrCmd->cmd) {
	case PA_ECNR_GET_VERSION_CFM:
		cout << "PA_ECNR_GET_VERSION_CFM" << endl;
		if (NULL == m_pVersion || NULL == *m_pVersion) {
			m_errCallBack("Version is NULL");
			return PA_ECNR_FAILED;
		}
		memcpy(*m_pVersion, ecnrCmd->data, sizeof(*m_pVersion));
		return (paErrCode)ecnrCmd->status;

	case PA_ECNR_SET_PARAM_CFM:
		cout << "PA_ECNR_SET_PARAM_CFM" << endl;
		if (m_acErr != NULL) {
			memcpy(m_acErr, ecnrCmd->acErr, sizeof(ecnrCmd->acErr));
		}
		return (paErrCode)ecnrCmd->status;

	case PA_ECNR_GET_PARAM_CFM:
		cout << "PA_ECNR_GET_PARAM_CFM" << endl;
		if (NULL == m_pParam) {
			return PA_ECNR_FAILED;
		}
		memcpy(m_pParam->data, ecnrCmd->data, sizeof(ecnrCmd->data));
		return (paErrCode)ecnrCmd->status;

	case PA_ECNR_GET_RUNNING_STATE_CFM:
		cout << "PA_ECNR_GET_RUNNING_STATE_CFM" << endl;
		m_runningState = (ecnrRunningState)ecnrCmd->state;
		return (paErrCode)ecnrCmd->status;
	
	case PA_ECNR_SET_MUTE_PATH_CFM:
		cout << "PA_ECNR_SET_MUTE_PATH_CFM" << endl;
		return (paErrCode)ecnrCmd->status;
		
	case PA_ECNR_SET_MIC_OUTPUT_GAIN_CFM:
		cout << "PA_ECNR_SET_MIC_OUTPUT_GAIN_CFM" << endl;
		return (paErrCode)ecnrCmd->status;
		
	case PA_ECNR_SET_AUDIO_BUFFER_SIZE_CFM:
		cout << "PA_ECNR_SET_AUDIO_BUFFER_SIZE_CFM" << endl;
		if ((paErrCode)ecnrCmd->status == PA_ECNR_FAILED) {
			IsSetBufferSize = false;
			setBufSize = -1;
		}
		else {
			IsSetBufferSize = true;
		}
		return (paErrCode)ecnrCmd->status;

	case PA_ECNR_RECORD_AUDIO_START_CFM:
		cout << "PA_ECNR_RECORD_AUDIO_START_CFM" << endl;
		memset(&m_recordFilePath, NULL, sizeof(m_recordFilePath));

		if (m_streamBitMask & RECORD_RECVIN_STREAM) {
			char commonName[MAX_FOLDER_PATH_LEN] = {0};
			char folder[MAX_FOLDER_PATH_LEN] = {0};
			memcpy(commonName, m_commonName, strlen(m_commonName));
			memcpy(folder, m_floder, strlen(m_floder));
			strcat_s(commonName, MAX_FOLDER_PATH_LEN, "RecvIn");
			strcat_s(folder, MAX_FOLDER_PATH_LEN, commonName);
			memcpy(m_recordFilePath.recvInPath, folder, sizeof(folder));
			fs.RecvIn.open(folder, fstream::out | fstream::binary | fstream::trunc);
			if (!(fs.RecvIn)) {
				// 打开文件失败
				m_errCallBack("open file failed");
				return PA_FILE_NOT_EXIT;
			}
			else {
				IsOpenRcdFs = true;
			}
			m_bIsRecordRecvIn = true;
		}

		if (m_streamBitMask & RECORD_RECVOUT_STREAM) {
			char commonName[MAX_FOLDER_PATH_LEN] = {0};
			char folder[MAX_FOLDER_PATH_LEN] = {0};
			memcpy(commonName, m_commonName, strlen(m_commonName));
			memcpy(folder, m_floder, strlen(m_floder));
			strcat_s(commonName, MAX_FOLDER_PATH_LEN, "RecvOut");
			strcat_s(folder, MAX_FOLDER_PATH_LEN, commonName);
			memcpy(m_recordFilePath.recvOutPath, folder, sizeof(folder));
			fs.RecvOut.open(folder, fstream::out | fstream::binary | fstream::trunc);
			if(!(fs.RecvOut)) {
				// 打开文件失败
				m_errCallBack("open file failed");
				return PA_FILE_NOT_EXIT;
			}
			else {
				IsOpenRcdFs = true;
			}
			m_bIsRecordRecvOut = true;
		}

		if (m_streamBitMask & RECORD_MICIN_STREAM) {
			char commonName[MAX_FOLDER_PATH_LEN] = {0};
			char folder[MAX_FOLDER_PATH_LEN] = {0};
			memcpy(commonName, m_commonName, strlen(m_commonName));
			memcpy(folder, m_floder, strlen(m_floder));
			strcat_s(commonName, MAX_FOLDER_PATH_LEN, "MicIn");
			strcat_s(folder, MAX_FOLDER_PATH_LEN, commonName);
			memcpy(m_recordFilePath.micInPath, folder, sizeof(folder));
			fs.MicIn.open(folder, fstream::out | fstream::binary | fstream::trunc);
			if(!(fs.MicIn)) {
				// 打开文件失败
				m_errCallBack("open file failed");
				return PA_FILE_NOT_EXIT;
			}
			else {
				IsOpenRcdFs = true;
			}
			m_bIsRecordMicIn = true;
		}

		if (m_streamBitMask & RECORD_MICOUT_STREAM) {
			char commonName[MAX_FOLDER_PATH_LEN] = {0};
			char folder[MAX_FOLDER_PATH_LEN] = {0};
			memcpy(commonName, m_commonName, strlen(m_commonName));
			memcpy(folder, m_floder, strlen(m_floder));
			strcat_s(commonName, MAX_FOLDER_PATH_LEN, "MicOut");
			strcat_s(folder, MAX_FOLDER_PATH_LEN, commonName);
			memcpy(m_recordFilePath.micOutPath, folder, sizeof(folder));
			fs.MicOut.open(folder, fstream::out | fstream::binary | fstream::trunc);
			if(!(fs.MicOut)) {
				// 打开文件失败
				m_errCallBack("open file failed");
				return PA_FILE_NOT_EXIT;
			}
			else {
				IsOpenRcdFs = true;
			}
			m_bIsRecordMicOut = true;
		}

		if (m_streamBitMask & RECORD_REFIN_STREAM) {
			char commonName[MAX_FOLDER_PATH_LEN] = {0};
			char folder[MAX_FOLDER_PATH_LEN] = {0};
			memcpy(commonName, m_commonName, strlen(m_commonName));
			memcpy(folder, m_floder, strlen(m_floder));
			strcat_s(commonName, MAX_FOLDER_PATH_LEN, "RefIn");
			strcat_s(folder, MAX_FOLDER_PATH_LEN, commonName);
			memcpy(m_recordFilePath.refInPath, folder, sizeof(folder));
			fs.RefIn.open(folder, fstream::out | fstream::binary | fstream::trunc);
			if(!(fs.RefIn)) {
				// 打开文件失败
				m_errCallBack("open file failed");
				return PA_FILE_NOT_EXIT;
			}
			else {
				IsOpenRcdFs = true;
			}
			m_bIsRecordRefIn = true;
		}
		return (paErrCode)ecnrCmd->status;
	
	case PA_ECNR_RECORD_AUDIO_STOP_CFM:
	
				
		cout << "PA_ECNR_RECORD_AUDIO_STOP_CFM" << endl;
		return (paErrCode)ecnrCmd->status;

	case PA_ECNR_INSEART_AUDIO_STREAM_CFM:
		cout << "PA_ECNR_INSEART_AUDIO_STREAM_CFM11" << endl;
		return (paErrCode)ecnrCmd->status;

	case PA_ECNR_RECORD_BUFFER_END:
		cout << "PA_ECNR_RECORD_BUFFER_END" << endl;
		fs.RecvIn.close();
		fs.RecvOut.close();
		fs.MicIn.close();
		fs.MicOut.close();
		fs.RefIn.close();
		IsOpenRcdFs = false;
				
		if (m_bIsRecordRecvIn) {
			PCMConvertToWAVFile(recvInPath);
		}
		if (m_bIsRecordMicIn) {
			PCMConvertToWAVFile(micInPath);
		}

		if (m_bIsRecordRefIn) {
			PCMConvertToWAVFile(refInPath);
		}
		if (m_bIsRecordRecvOut) {
			PCMConvertToWAVFile(recvOutPath);
		}

		if (m_bIsRecordMicOut) {
			PCMConvertToWAVFile(micOutPath);
		}

		m_bIsRecordRecvIn = false;
		m_bIsRecordMicIn = false;
		m_bIsRecordRefIn = false;
		m_bIsRecordRecvOut = false;
		m_bIsRecordMicOut = false;
		return PA_ECNR_OK;
		break;

	default:
		break;
	}
	m_errCallBack("Unknown command");
	return PA_ECNR_FAILED;
}

void* pa_ecnr_pluginImpl::paEcnrRegErrorCallback(ecnr_errorCallBack errorCB)
{
	m_errorCB = errorCB;
	return NULL;
}

paErrCode pa_ecnr_pluginImpl::paEcnrDisconnectServer()
{
	m_errCallBack("Disconnect");
	
	if (m_bConnectState) {
		paECNR_Cmd ecnrCmd;
		char data[SEND_RECV_BUFFER_MAXSIZE] = {0};
		ecnrCmd.cmd = PA_ECNR_DISCONNECT;
		memcpy(data, &ecnrCmd, SEND_RECV_BUFFER_MAXSIZE);

		send(SocketClientCommand, data, sizeof(data), 0);
		send(SocketClientInsert, data, sizeof(data), 0);
	}
	m_bConnectState = false;

	//paEcnrStopRecordAudioStream();

	m_pVersion = NULL;
	m_pParam = NULL;
	m_runningState = PA_ECNR_NOT_RUNNING;

	m_streamBitMask = NULL;
	m_acErr = NULL;

	fs.RecvIn.close();
	fs.RecvOut.close();
	fs.MicIn.close();
	fs.MicOut.close();
	fs.RefIn.close();

	m_InstFs.close();

	IsOpenInstFs = false;
	IsOpenRcdFs = false;
	IsStart = true;
	
	WSACleanup();
	//判断线程是否退出
	if (IsDestroyThread) {
		if(m_hThreadRecord)
		{
			SetEvent(m_hQuitEventRecord);
			if(WAIT_OBJECT_0 != WaitForSingleObject(m_hThreadCheckRecord,10))
			{
				TerminateThread(m_hThreadRecord,-1);
			}
		}
		if(m_hThreadInsert)
		{
			SetEvent(m_hQuitEventInsert);
			if(WAIT_OBJECT_0 != WaitForSingleObject(m_hThreadCheckInsert,10))
			{
				TerminateThread(m_hThreadInsert,-1);
			}
		}
		if(m_hQuitEventRecord)
		{
			CloseHandle(m_hQuitEventRecord);
			m_hQuitEventRecord = NULL;
		}
		if(m_hThreadCheckRecord)
		{
			CloseHandle(m_hThreadCheckRecord);
			m_hThreadCheckRecord = NULL;
		}
		if(m_hThreadRecord)
		{
			CloseHandle(m_hThreadRecord);
			m_hThreadRecord = NULL;
		}
		if(m_hQuitEventInsert)
		{
			CloseHandle(m_hQuitEventInsert);
			m_hQuitEventInsert = NULL;
		}
		if(m_hThreadCheckInsert)
		{
			CloseHandle(m_hThreadCheckInsert);
			m_hThreadCheckInsert = NULL;
		}
		if(m_hThreadInsert)
		{
			CloseHandle(m_hThreadInsert);
			m_hThreadInsert = NULL;
		}
		IsDestroyThread = false;
	}

	if (SocketClientCommand) {
		closesocket(SocketClientCommand);
		SocketClientCommand = NULL;
	}
	if (SocketClientRecord) {
		closesocket(SocketClientRecord);
		SocketClientRecord = NULL;
	}
	if (SocketClientInsert) {
		closesocket(SocketClientInsert);
		SocketClientInsert = NULL;
	}

	return PA_ECNR_OK;
}

void pa_ecnr_pluginImpl::m_errCallBack(char * reason) 
{
	if (m_errorCB) {
		m_errorCB(reason);
	}
}

int pa_ecnr_pluginImpl::ConvertToWavFile(FILE *fp, long filelength)
{
	if (fp == NULL)
    {
        return -1;
    }

	//写WAV文件头  
	RIFF_HEADER rh;
	memset(&rh, 0, sizeof(rh));
	memcpy(rh.szRiffFormat, "WAVE", 4);
	memcpy(rh.szRiffID, "RIFF", 4);
  
	fwrite(&rh, 1, sizeof(rh), fp);

	FMT_BLOCK fb;
	memcpy(fb.szFmtID, "fmt ", 4);
	fb.dwFmtSize = 16;
	fb.wavFormat.wFormatTag = 1;
	fb.wavFormat.wChannels = 1;
	fb.wavFormat.wBitsPerSample = 16;
	fb.wavFormat.dwSamplesPerSec = 16000;
	fb.wavFormat.wBlockAlign = fb.wavFormat.wChannels * fb.wavFormat.wBitsPerSample / 8; //4;
	fb.wavFormat.dwAvgBytesPerSec = fb.wavFormat.dwSamplesPerSec * fb.wavFormat.wBlockAlign;

	fwrite(&fb, 1, sizeof(fb), fp);
	char buf[] = { "data" };
	fwrite(buf, 1, sizeof(buf), fp);

	//更新WAV文件dwRiffSize字段中的值  
    int nWhere = 4;
	DWORD_CHAR dc;
	//writeFile2Int
    fseek(fp, nWhere, SEEK_SET);  
    dc.nValue = filelength + 40;  
    fwrite(dc.charBuf, 1, 4, fp);  

    //更新WAV文件DataChunk中Size字段的值
    nWhere = sizeof(RIFF_HEADER) + sizeof(FMT_BLOCK) + 4;
	//writeFile2Int
    fseek(fp, nWhere, SEEK_SET);
	memset(&dc, NULL, sizeof(dc));
    dc.nValue = filelength;
    fwrite(dc.charBuf, 1, 4, fp);

	return 0;
}