	/***************************************************************************
Project:                XXXXXXX
(c) Copyright:       XXXXXXX
Company:            XXXXXXX

****************************************************************************/

/***************************************************************************
@File:                     pa_ecnr_err.h
@Description:          XXXXXXX

****************************************************************************/

#ifndef PA_ECNR_ERR_H
#define PA_ECNR_ERR_H

#ifdef __cplusplus
    extern "C" {
#endif

/*******************************Error Codes********************************/
#define  PA_SYSTEM_ERROR_START                 0x0010
#define  PA_FILE_ERROR_START                       0x0011

/*Following defined the detail errorcode*/
typedef enum{
    PA_ECNR_OK = 0x0001,
    PA_ECNR_FAILED = PA_SYSTEM_ERROR_START,
    PA_FILE_NOT_EXIT = PA_FILE_ERROR_START
}paErrCode;


#ifdef __cplusplus
    }
#endif

#endif //PA_ECNR_ERR_H
//Eof
