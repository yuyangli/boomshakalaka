#include <iostream>
//#include "stdafx.h"
#include "pa_ecnr_plugin.h"
#include "pa_ecnr_pluginImpl.h"

#ifdef __cplusplus
	extern "C" {
#endif

paErrCode  paEcnrConnectServer(const ipaddress  rem_ip, const unsigned short port)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();

	if (NULL == Inst) {
		return PA_ECNR_FAILED;
	}
	return Inst->paEcnrConnectServer(rem_ip, port);
}

paErrCode  paEcnrDisconnectServer()
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return PA_ECNR_FAILED;
	}
	return Inst->paEcnrDisconnectServer();
}

bool paEcnrGetConStatus(void)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return false;
	}
	return Inst->paEcnrGetConStatus();
}

void* paEcnrRegErrorCallback(ecnr_errorCallBack errorCB)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return NULL;
	}
	return Inst->paEcnrRegErrorCallback(errorCB);
}

ecnrRunningState  paEcnrGetRemProcess(void)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return PA_ECNR_NOT_INIT;
	}
	return Inst->paEcnrGetRemProcess();
}

paErrCode  paEcnrGetVersion(paEcnrVersion* version)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return PA_ECNR_FAILED;
	}
	return Inst->paEcnrGetVersion(version);
}

paErrCode  paEcnrSetParam(paECNRParam* param, char* acErr)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return PA_ECNR_FAILED;
	}
	return Inst->paEcnrSetParam(param, acErr);
}

paErrCode  paEcnrGetParam(paECNRParam* param)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return PA_ECNR_FAILED;
	}
	return Inst->paEcnrGetParam(param);
}

//paErrCode  paEcnrExportCfgFile(paECNRParam* param, const char* file)
//{
//	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
//	if (NULL == Inst) {
//		return PA_ECNR_FAILED;
//	}
//	return Inst->paEcnrExportCfgFile(param, file);
//}
//
//paErrCode  paEcnrImportCfgFile(const char* file,paECNRParam* param)
//{
//	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
//	if (NULL == Inst) {
//		return PA_ECNR_FAILED;
//	}
//	return Inst->paEcnrImportCfgFile(file, param);
//}

paErrCode  paEcnrMuteAudioPath(audio_path audioPath)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return PA_ECNR_FAILED;
	}
	return Inst->paEcnrMuteAudioPath(audioPath);
}

paErrCode  paEcnrSetMicGain(float gain)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return PA_ECNR_FAILED;
	}
	return Inst->paEcnrSetMicGain(gain);
}

paErrCode  paEcnrSetRemoteStreamBufSize(const int bufSize)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return PA_ECNR_FAILED;
	}
	return Inst->paEcnrSetRemoteStreamBufSize(bufSize);
}

int paEcnrGetLeftBufSize( audio_path audioPath)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return -1;
	}
	return Inst->paEcnrGetLeftBufSize(audioPath);
}

paErrCode  paEcnrStartRecordAudioStream(const char* folder, char streamBitMask, const char* commonName)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return PA_ECNR_FAILED;
	}
	return Inst->paEcnrStartRecordAudioStream(folder, streamBitMask, commonName);
}

paErrCode  paEcnrStopRecordAudioStream(void)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return PA_ECNR_FAILED;
	}
	return Inst->paEcnrStopRecordAudioStream();
}

paErrCode  paEcnrInseartAudioStream(const char* fllePath, audio_path audioPath)
{
	pa_ecnr_pluginImpl *Inst = pa_ecnr_pluginImpl::getPa_ecnr_pluginImplInst();
	if (NULL == Inst) {
		return PA_ECNR_FAILED;
	}
	return Inst->paEcnrInseartAudioStream(fllePath, audioPath);
}

#ifdef __cplusplus
	}
#endif
